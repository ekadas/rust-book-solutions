# Rust Book Solutions
> My solutions to exercises in the rust book

## Usage
Exercises could be run with the use of [structopt](https://docs.rs/structopt/0.3.3/structopt/).
Run:
```sh
cargo run -- <chapter> <exercise> <addional arguments>
```
Where `<chapter>` is an integer denoting the chapter,
`exercise` is an integer denoting the exercise in the chapter
and `<addinal arguments>` depends on the exercise (run `--help` to find out more).
