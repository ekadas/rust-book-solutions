use structopt::StructOpt;

mod chapter_8;

#[derive(Debug, StructOpt)]
#[structopt(name = "demo", about = "demoes the solutions to exercises")]
enum Chapter {
    #[structopt(name = "8")]
    /// The eighth chapter in the book
    Eight(chapter_8::Eight)
}

impl Chapter {
    fn run(self) {
        match self {
            Chapter::Eight(chapter) => chapter.run()
        }
    }
}

fn main() {
    let opt = Chapter::from_args();
    opt.run();
}
