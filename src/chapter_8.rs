use structopt::StructOpt;

mod exercise_1;
mod exercise_2;
mod exercise_3;

#[derive(Debug, StructOpt)]
pub enum Eight {
    #[structopt(name = "1")]
    /// The first exercise in the chapter
    One {
        #[structopt(short, long, help = "space separated integers to do calculations on", required(true))]
        vector: Vec<i32>
    },
    #[structopt(name = "2")]
    /// The second exercise in the chapter
    Two {
        #[structopt(short, long, help = "string to convert to pig latin", required(true))]
        string: String
    },
    #[structopt(name = "3")]
    /// The third exercise in the chapter
    Three
}

impl Eight {
    pub fn run(self) {
        match self {
            Eight::One { vector } => exercise_1::run(vector),
            Eight::Two { string } => exercise_2::run(string),
            Eight::Three => exercise_3::run()
        }
    }
}
