pub fn run(string: String) {
    let character = match string.chars().next() {
        Some(character) => character,
        _ => {
            println!("Invlid string");
            return
        }
    };
    if is_vowel(character) {
        println!("{}-hay", string);
    } else {
        let mut copy = String::from(string);
        let first = copy.remove(0);
        println!("{}-{}ay", copy, first);
    }
}

fn is_vowel(character: char) -> bool {
    let c: &str = &character.to_lowercase().to_string();
    match c {
        "a" | "e" | "i" | "o" | "u" => true,
        _ => false
    }
}
