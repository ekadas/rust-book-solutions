use std::collections::hash_map::HashMap;
use std::io;

pub fn run() {
    let mut structure: HashMap<String, Vec<String>> = HashMap::new();

    loop {
        println!("Please provide a command");
        let mut command = String::new();

        io::stdin().read_line(&mut command)
            .expect("Failed to read command");

        let command: Vec<_> = command.trim().split(' ').collect();
        match command.get(0) {
            Some(&"Add") => add(command.clone(), &mut structure),
            Some(&"List") => list(command.clone(), &structure),
            _ => println!("Invalid command: {:?}", command)
        }
    }
}

fn add(command: Vec<&str>, structure: &mut HashMap<String, Vec<String>>) {
    match command.get(1) {
        Some(&name) => {
            match command.get(3) {
                Some(&department) => {
                    let names = structure.entry(String::from(department)).or_insert(Vec::new());
                    names.push(String::from(name));
                },
                _ => println!("No such department")
            }
        },
        _ => println!("No name provided")
    }
}

fn list(command: Vec<&str>, structure: &HashMap<String, Vec<String>>) {
    match command.get(1) {
        Some(&department) => {
            match structure.get(&String::from(department)) {
                Some(list) => {
                    println!("{}", vec_alphabetical(list));
                },
                _ => println!("Department does not exist")
            }
        },
        _ => println!("{}", hash_alphabetical(structure))
    }
}

fn vec_alphabetical(vec: &Vec<String>) -> String {
    let mut output = String::new();
    let mut sorted = vec.clone();
    sorted.sort();
    for name in &sorted[0..sorted.len() - 1] {
        output.push_str(&format!("{}, ", name));
    }

    output.push_str(&sorted[sorted.len() - 1]);
    output
}

fn hash_alphabetical(hash: &HashMap<String, Vec<String>>) -> String {
    let mut output = String::new();
    let mut sorted: Vec<String> = hash.keys().cloned().collect();
    sorted.sort();
    for key in &sorted {
        output.push_str(&format!("{} -> {}\n", key, vec_alphabetical(&hash[key])));
    }
    output
}
