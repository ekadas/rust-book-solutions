use std::collections::HashMap;

pub fn run(v: Vec<i32>) {
    let mode = calculate_mode(&v);
    let median = calculate_median(&v);
    let mean = calculate_mean(&v);
    println!("Mean: {}\nMedian: {}\nMode: {}", mean, median, mode);
}

fn calculate_mode(v: &Vec<i32>) -> i32 {
    let mut map = HashMap::new();

    for i in v {
        let count = map.entry(*i).or_insert(0);
        *count += 1;
    }

    *map.iter().max_by_key(|&(_, value)| value).unwrap().0
}

fn calculate_mean(v: &Vec<i32>) -> i32 {
    let size: i32 = v.len() as i32;
    let total: i32 = v.iter().sum();
    total / size
}

fn calculate_median(v: &Vec<i32>) -> i32 {
    let size = v.len();
    v[size / 2]
}
